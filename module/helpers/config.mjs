export const ENGRENAGES = {};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 ENGRENAGES.abilities = {
  "str": "ENGRENAGES.AbilityStr",
  "dex": "ENGRENAGES.AbilityDex",
  "con": "ENGRENAGES.AbilityCon",
  "int": "ENGRENAGES.AbilityInt",
  "wis": "ENGRENAGES.AbilityWis",
  "cha": "ENGRENAGES.AbilityCha"
};

ENGRENAGES.abilityAbbreviations = {
  "str": "ENGRENAGES.AbilityStrAbbr",
  "dex": "ENGRENAGES.AbilityDexAbbr",
  "con": "ENGRENAGES.AbilityConAbbr",
  "int": "ENGRENAGES.AbilityIntAbbr",
  "wis": "ENGRENAGES.AbilityWisAbbr",
  "cha": "ENGRENAGES.AbilityChaAbbr"
};